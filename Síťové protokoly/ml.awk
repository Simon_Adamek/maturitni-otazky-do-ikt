BEGIN{g=0; str="\\begin{itemize}"}
/\s*[\*\+-]\s/ {
	f=length($0)
	sub(/^\s*/,"",$0)
	if (f-length($0)>g){
		str=str "\\begin{itemize}\n"
		g=f-length($0)
	}
	if (f-length($0)<g){
		str=str "\\end{itemize}\n"
		g=f-length($0)
	}
	kop=$0
	sub(/^\*\s/, "", kop)
	str=str " \\item " kop "\n"
	pr=1
	}

/^[^\*\+-]/ {
	if (pr=1 && length($0)>0){
		str=str "\\end{itemize}\n"
	}
	pr=0
}


END{
	if (pr=1){
		for (i=0; i<=g; i++)
			str=str "\\end{itemize}\n"
		pr=0
	}
	print(str)
}
