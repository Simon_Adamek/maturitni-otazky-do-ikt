#/usr/bin/env python3

samohlasky={'i', 'í', 'e', 'é', 'a', 'á', 'o', 'ó', 'u', 'ú', 'y', 'ý', 'ů'}

text=input("Zadejte větu, zadání ukončete tečkou: ")

if text[-1] == ".":
    print(len([x for x in text.lower() if x in samohlasky]))
else:
    print("Špatný formát!")
