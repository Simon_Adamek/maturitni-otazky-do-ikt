
def nacti_matici(rozmer):
    if rozmer < 2:
        print("Rozměr musí být alespoň 2")
        return

    matice = [[] for i in range(rozmer)]
    for i in range(rozmer):
        for j in range(rozmer):
            matice[i].append(int(input("Zadej číslo na řádku {} a sloupci {}: ".format(i+1, j+1))))
    return matice


def soucet(maticeA, maticeB):
    for i in range(len(maticeA)):
        for j in range(len(maticeA)):
            maticeA[i][j]+=maticeB[i][j]
    return maticeA

rozmer = int(input("zadej rozměr: "))

print("Zadej hodnoty první matice: ", end="\n\n")
matice1 = nacti_matici(rozmer)

print()

print("Zadej hodnoty druhé matice: ", end="\n\n")
matice2 = nacti_matici(rozmer)

matice = soucet(matice1, matice2)

print()
for i in range(rozmer):
    for j in range(rozmer):
        print("{0:^5s}".format(str(matice[i][j])), end="")
    print()

