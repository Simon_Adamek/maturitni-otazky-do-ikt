cislo=int(input("Číslo: "))
hexa = ""


while cislo > 0:
    zb = cislo%16
    if zb >= 10:
        hexa += chr(55+zb)
    else:
        hexa += str(zb)
    cislo = cislo//16

print(hexa[::-1])
