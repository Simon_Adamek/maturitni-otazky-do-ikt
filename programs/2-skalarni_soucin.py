import sys

def get_vector(text):
    while True:
        try:
            return list(map(int, input("\n"+text).strip("()").split(",")))
        except ValueError:
            print("Souřadnice vektoru musí být ČÍSELNÉ hodnoty!")

def dot_product(u, v):
    if len(u) != len(v):
        print("\nOba vektory musejí mít stejný rozměr")
        return 
    if len(u)<2:
        print("\n Vektor má alespoň dvě souřadnice")
        return
    return sum(u[x]*v[x] for x in range(len(u)))
    

if len(sys.argv) > 1:
    print("""
Program pro výpočet skalárního součinu dvou vektorů.

----------------------------------------------------

- vektory lze zadat v následujících formátech - (1,1) nebo 1,1 nebo 1, 1
- program se ukončí stiskem kláves <Ctrl>+d
- rozměr vektorů není omezen, ale u obou zadaných musí být stejný

----------------------------------------------------

Autor: Šimon Adámek@2023

Chyby, připomínky nebo nápady na vylepšení můžete posílat na email <simon.adamek@centrum.cz>

----------------------------------------------------

Program je vytvořený jako svobodný software, můžete ho dále šířit nebo upravovat za podmínek stanovených GNU General Public Licence version 3 (viz <www.gnu.org/licences>)"""
)

while True:
    try:
        result = str(dot_product(get_vector("u: "), get_vector("v: ")))
        if result:
            print("\nu•v= " + result)
        print("\n"+"-"*50)
    except EOFError:
        print()
        break


