
cislo = input("Zadej rodné číslo: ")

#rok mesic den cc 
rc = [cislo[:2], cislo[2:4], cislo[4:6], cislo[7:12]]

if sum(map(int, rc)) % 11 == 0:
    if int(rc[1]) > 12:
        rc[1] -= 50

    if int(rc[0]) <= 23:
        rc[0] = "20" + rc[0]
    else:
        rc[0] = "19" + rc[0]

print("Datum je {}.{}.{}".format(*rc[2::-1]))
