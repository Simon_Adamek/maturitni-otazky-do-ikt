import cmath
import math

def get_int(text):
    while True:
        try:
            return int(input(text))
        except ValueError:
            pass

print("Kvadratická rovnice má tvar ax^2 + bx + c = 0, zadávejte prosím postupně jednotlivé členy: ")

a = get_int("Člen a: ")
b = get_int("Člen b: ")
c = get_int("Člen c: ")

D = b**2-4*a*c


if D == 0:
    print("Diskriminant je 0, rovnice má jedinné řešení: {}.".format(round(-b/(2*a), 2)))
elif D > 0:
    D = math.sqrt(D)
    print("Rovnice má dva reálné kořeny {} a {}.".format(round((-b+D)/(2*a), 2), round((-b-D)/(2*a),2)))
elif D < 0:
    D = cmath.sqrt(D)
    print("Rovnice má dvě řešení v oboru komplexních čísel: {} a {}".format((-b+D)/(2*a), (-b-D)/(2*a)))
