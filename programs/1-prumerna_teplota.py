#!/usr/bin/env python3

def get_int(text):
    while True:
        num = input(text)
        if num.isnumeric():
            return int(num) 

while True:
    pocet = get_int("Zadej požadovaný počet teplot: ")

    print()

    prumer = 0

    for i in range(1,pocet+1):
        prumer += get_int("Zadej teplotu číslo {}: ".format(i))

    print("\nPrůměr těchto teplot je: {}".format(round(prumer/pocet, 2)), end="\n\n")

    if input("Chceš zadat další teploty (a/n)?: ").lower() != 'a':
        break

    print("\n"+"-"*50, end="\n\n")
